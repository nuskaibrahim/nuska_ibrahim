import React,{useState} from 'react';
import { View, StyleSheet, Image, Text, TextInput, TouchableOpacity, Alert } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Login from './src/SubPages/Login';
import CreateAccount from './src/SubPages/CreateAccount';
import SettingScreen from './src/SubPages/SettingScreen';


function HomeScreen({navigation,route}){
  const {rece}=route.params;
  return(
    <View style={Styles.container}>
      <Text>Home Screen</Text>
      <Text>Hi.. {rece}</Text>
    </View>
  );
}

const tabs=createBottomTabNavigator();
function TabScreen({route}){
  const {receved}=route.params;
  return(
    <tabs.Navigator>
      <tabs.Screen name="Home" component={HomeScreen} initialParams={{rece:receved}}/>
      <tabs.Screen name="Setting" component={SettingScreen}/>
    </tabs.Navigator>
  );
}

const  stack=createStackNavigator();
function App(){
  return(
      <NavigationContainer>
        <stack.Navigator>
          <stack.Screen name='Registration' component={CreateAccount}/>
          <stack.Screen name='Home' component={TabScreen}/>
          <stack.Screen name='Login' component={Login}/>
        </stack.Navigator>
      </NavigationContainer>
  );
}

export default App;

const Styles=StyleSheet.create({

  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'rgba(255,255,255,1)',
  },
  textInput:{
    padding:10,
    marginBottom:10,
    backgroundColor:'rgba(0,0,0,0.5)',
    color:'rgba(255,255,255,1)',
    width:200
  },
  btn:{
    padding:10,
    backgroundColor:'rgba(0,0,150,1)',
    width:200,
    alignItems:'center'
  },
  btntxt:{
    color:'rgba(255,255,255,1)',
  }
});