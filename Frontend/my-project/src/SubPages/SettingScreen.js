import React,{useState} from 'react';
import { View, StyleSheet, Image, Text, TextInput, TouchableOpacity, Alert } from 'react-native';

export default function CreateAccount({navigation}) {
    const [fullName,SetFN]=useState('');
    const [email,SetEM]=useState('');
    const [userName,SetUN]=useState('');
    const [password,SetPW]=useState('');
    const [cPassword,SetCPW]=useState('');
    
    
    return(
      <View style={Styles.container}>
          <View>
              <Image 
                style={Styles.imgs}
                source={require('../img/us2.png')}
              />
          </View>
            <View>
                <TextInput
                placeholder="Full Name"
                placeholderTextColor="rgba(255,255,255,1)"
                returnKeyType='next'
                style={Styles.textInput}
                onChangeText={fullName=>SetFN(fullName)}
                />
            </View>
            <View>
            <TextInput
              placeholder="Email"
              placeholderTextColor="rgba(255,255,255,1)"
              returnKeyType='next'
              style={Styles.textInput}
              onChangeText={email=>SetEM(email)}
            />
            </View>
            <View>
            <TextInput
              placeholder="User Name"
              placeholderTextColor="rgba(255,255,255,1)"
              returnKeyType='next'
              style={Styles.textInput}
              onChangeText={userName=>SetUN(userName)}
            />
            </View>

          <View>
            <TextInput
              placeholder="Password"
              placeholderTextColor="rgba(255,255,255,1)"
              returnKeyType='next'
              style={Styles.textInput}
              onChangeText={password=>SetPW(password)}
              secureTextEntry
            />
          </View>

          <View>
            <TextInput
              placeholder="Confirm Password"
              placeholderTextColor="rgba(255,255,255,1)"
              returnKeyType='go'
              style={Styles.textInput}
              onChangeText={cPassword=>SetCPW(cPassword)}
              secureTextEntry
  
            />
          </View>
  
          <View>
            <TouchableOpacity style={Styles.btn}
              onPress={()=>{
                if(fullName.length==0 ||email.length==0 ||userName.length==0 ||password.length==0 ||cPassword.length==0){
                  Alert.alert('Requried filed is missing.!');
                }
                else{
                    if(password==cPassword){
                        var apiURI="http://127.0.0.1:5000/my_app/insert.php";
                        
                        var header={
                            'Accept':'application/json',
                            'Content-Type':'application.json'
                        };
                        var data={
                            fullName:fullName,
                            email:email,
                            userName:userName,
                            password:password
                        };
                        fetch(apiURI,{
                            method:'POST',
                            headers:header,
                            body: JSON.stringify(data)
                        })
                        .then((response)=>response.json())
                        .then((response )=>{Alert.alert(response[0].Message);})
                        .catch((error)=>{Alert.alert("Error"+error);console.log("Error"+error);})
                        
                        //navigation.navigate('Login')
                        //console.log(data);
                    }
                }
              }}
            ><Text style={Styles.btntxt}>Create New Account</Text></TouchableOpacity>
          </View>
          <View style={Styles.txtNew}>
            <TouchableOpacity 
              onPress={()=>{ navigation.navigate('Login')}}
            ><Text >If you have account ? Login here</Text></TouchableOpacity>
          </View>
  
        </View>
    );
}


const Styles=StyleSheet.create({

  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'rgba(0,0,0,0.1)',
  },
  textInput:{
    padding:10,
    marginBottom:10,
    backgroundColor:'rgba(0,0,0,0.8)',
    borderRadius:10,
    color:'rgba(255,255,255,1)',
    width:300
  },
  btn:{
    padding:10,
    backgroundColor:'rgba(0,150,0,1)',
    width:300,
    borderRadius:10,
    alignItems:'center'
  },
  btntxt:{
    color:'rgba(255,255,255,1)',
  },
  txtNew : {
    marginTop:10,
  },
  imgs:{
    width:120,
    height:120,
    marginBottom:20
  },
});
