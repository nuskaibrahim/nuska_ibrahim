import React,{useState} from 'react';
import { View, StyleSheet, Image, Text, TextInput, TouchableOpacity, Alert } from 'react-native';

export default function Login({navigation}) {
    const [userName,SetUN]=useState('');
    const [password,SetPW]=useState('');
    
    return(
      <View style={Styles.container}>
          <View>
              <Image 
              style={Styles.imgs}
                source={require('../img/us1.png')}
              />
          </View>
          <View>
            <TextInput
              placeholder="Enter User Name"
              placeholderTextColor="rgba(255,255,255,1)"
              returnKeyType='next'
              style={Styles.textInput}
              onChangeText={userName=>SetUN(userName)}
  
            />
          </View>
  
          <View>
            <TextInput
              placeholder="Enter Password"
              placeholderTextColor="rgba(255,255,255,1)"
              returnKeyType='go'
              style={Styles.textInput}
              onChangeText={password=>SetPW(password)}
              secureTextEntry
  
            />
          </View>
  
          <View>
            <TouchableOpacity style={Styles.btn}
              onPress={()=>{
                var apiURI="http://127.0.0.1:80/my_app/login.php";
                        
                var header={
                    'Accept':'application/json',
                    'Content-Type':'application.json'
                };
                var data={
                    userName:userName,
                    password:password
                };
                fetch(apiURI,{
                    method:'POST',
                    headers:header,
                    body: JSON.stringify(data)
                })
                .then((response)=>response.json())
                .then((response )=>{
                    Alert.alert(response[0].Message);
                    if(response[1]==userName && response[2]==true){
                        navigation.navigate('Home',{receved:userName})
                    }
                })
                .catch((error)=>{Alert.alert("Error"+error);console.log("Error"+error);})
                
                
                //console.log(data);
              }}
            ><Text style={Styles.btntxt}>login</Text></TouchableOpacity>
          </View>
          <View style={Styles.txtNew}>
            <TouchableOpacity 
              onPress={()=>{ navigation.navigate('Registration')}}
            ><Text >Don't have account ? Create New</Text></TouchableOpacity>
          </View>
  
        </View>
    );
}


const Styles=StyleSheet.create({

  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'rgba(255,255,255,1)',
  },
  textInput:{
    padding:10,
    marginBottom:10,
    backgroundColor:'rgba(0,0,0,0.8)',
    borderRadius:10,
    color:'rgba(255,255,255,1)',
    width:300
  },
  btn:{
    padding:10,
    backgroundColor:'rgba(0,0,150,1)',
    width:300,
    borderRadius:10,
    alignItems:'center'
  },
  btntxt:{
    color:'rgba(255,255,255,1)',
  },
  txtNew : {
    marginTop:10,
  },
  imgs:{
    width:120,
    height:120,
    marginBottom:20
  },
});
